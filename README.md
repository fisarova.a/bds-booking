# My BDS Project - Booking

<!-- ABOUT THE PROJECT -->
## About The Project

This project is a school project on the theme of hotel booking.

The idea: a database of a hotel chain in which employees can mostly follow the action of a hotel. How many guests does the hotel have? information on which room the guest is in and whether they want any service. And maybe what is the hotel rating and room availability etc ... 

There are about 7 windows. The basic CRUD operations (Create, Read, Update, Delete) and a simulate attack injection (still in progress).

### Installation

1. Download/Clone the project
2. Go to the directory and open bash\
3. Type:
```bash
$ mvn clean install
```
3. And then type:
```bash
$ java -jar target/bds-booking-1.0-SNAPSHOT.jar

```
## Sign-in with the following credentials:

User Login: Luky123

Password: heslo

## Screens from application: 

Login:

![login](/uploads/6c56a60168f8b10573427b8acc26e82e/login.PNG)


Main window: 

![booking](/uploads/1313449f2f35176a88eea1b56a8fbebc/booking.PNG)


Create:

![create](/uploads/2fc27b10a585d64769f956cdfecbb340/create.PNG)


Read:

![read](/uploads/b05aef4f66e52515506961bab854a31b/read.PNG)


Update:

![update](/uploads/b790b3a3d6fea16a17ee5f36fa503c7b/update.PNG)


Delete:

![delete](/uploads/e212ebe65983d6df4eb7e8de92fd3788/delete.PNG)


Simulate Injection Attack:

![attack](/uploads/ebf9718589d9c1faa8aa0114ce4abb74/attack.PNG)

