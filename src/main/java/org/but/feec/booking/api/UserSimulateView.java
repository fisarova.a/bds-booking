package org.but.feec.booking.api;

public class UserSimulateView {
    private String sqlCommand;
    private String sqlFixedCommand;

    public String getSqlCommand() {
        return sqlCommand;
    }

    public void setSqlCommand(String sqlCommand) {
        this.sqlCommand = sqlCommand;
    }

    public String getSqlFixedCommand() {
        return sqlFixedCommand;
    }

    public void setSqlFixedCommand(String sqlFixedCommand) {
        this.sqlFixedCommand = sqlFixedCommand;
    }

    @Override
    public String toString() {
        return "UserSimulateView{" +
                "sqlCommand='" + sqlCommand + '\'' +
                 ", sqlFixedCommand='" + sqlFixedCommand + '\'' +
                '}';
    }
}
