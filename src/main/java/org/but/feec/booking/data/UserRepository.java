package org.but.feec.booking.data;

import org.but.feec.booking.api.*;
import org.but.feec.booking.config.DataSourceConfig;
import org.but.feec.booking.exceptions.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserRepository {
    private static final Logger logger = LoggerFactory.getLogger(UserRepository.class);

    public UserAuthView findPersonByLogin(String login) {
        logger.debug("findPersonByLogin: " + login);
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT user_login, user_password" +
                             " FROM booking.users u" +
                             " WHERE u.user_login = ?")) {
            preparedStatement.setString(1, login);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    UserAuthView user = new UserAuthView();
                    user.setLogin(resultSet.getString("user_login"));
                    user.setPassword(resultSet.getString("user_password"));
                    return user;
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find person by ID with addresses failed.", e);
        }
        return null;
    }

    public void createGuest(UserCreateView userCreateView) {
        String insertGuestSQL = "INSERT INTO booking.guests (name, surname, date_of_birth) VALUES (?,?,?)";
        try (Connection connection = DataSourceConfig.getConnection();
             // would be beneficial if I will return the created entity back
             PreparedStatement preparedStatement = connection.prepareStatement(insertGuestSQL, Statement.RETURN_GENERATED_KEYS)) {
            // set prepared statement variables
            preparedStatement.setString(1, userCreateView.getName());
            preparedStatement.setString(2, userCreateView.getSurname());
            preparedStatement.setDate(3, userCreateView.getDateOfBirth());

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows == 0) {
                throw new DataAccessException("Creating guest failed, no rows affected.");
            }
        } catch (SQLException e) {
            throw new DataAccessException("Creating guest failed operation on the database failed.");
        }
    }

    public void deleteGuest(UserDeleteView userDeleteView) {
        String insertGuestSQL = "DELETE FROM booking.guests WHERE guest_id = ? and name = ? and surname = ?";
        String checkIfExists = "SELECT name,surname FROM booking.guests g WHERE g.guest_id = ?";
        try (Connection connection = DataSourceConfig.getConnection();
             // would be beneficial if I will return the created entity back
             PreparedStatement preparedStatement = connection.prepareStatement(insertGuestSQL, Statement.RETURN_GENERATED_KEYS)) {
            // set prepared statement variables
            preparedStatement.setLong(1, userDeleteView.getId());
            preparedStatement.setString(2, userDeleteView.getName());
            preparedStatement.setString(3, userDeleteView.getSurname());

            try {
                connection.setAutoCommit(false); //autoCommit false
                try (PreparedStatement ps = connection.prepareStatement(checkIfExists, Statement.RETURN_GENERATED_KEYS)) {
                    ps.setLong(1, userDeleteView.getId());
                    ps.execute();
                } catch (SQLException e) {
                    throw new DataAccessException("This guest for delete do not exists.");
                }
                int affectedRows = preparedStatement.executeUpdate();

                if (affectedRows == 0) {
                    throw new DataAccessException("Deleting guest failed, no rows affected.");
                }
                connection.commit();    //Commit transaction
            } catch (SQLException e) {
                connection.rollback();  //rollback transaction
            } finally {
                connection.setAutoCommit(true); //autoCommit true
            }
        } catch (SQLException e) {
            throw new DataAccessException("Deleting person failed operation on the database failed.");
        }

    }
    public void updateGuest(UserUpdateView userUpdateView) {
        String insertGuestSQL = "UPDATE booking.guests g SET name = ?, surname = ?, date_of_birth = ? WHERE g.guest_id = ?";
        String checkIfExists = "SELECT name,surname FROM booking.guests g WHERE g.guest_id = ?";
        try (Connection connection = DataSourceConfig.getConnection();
             // would be beneficial if I will return the created entity back
             PreparedStatement preparedStatement = connection.prepareStatement(insertGuestSQL, Statement.RETURN_GENERATED_KEYS)) {
            // set prepared statement variables
            preparedStatement.setString(1, userUpdateView.getName());
            preparedStatement.setString(2, userUpdateView.getSurname());
            preparedStatement.setDate(3, userUpdateView.getDateOfBirth());
            preparedStatement.setLong(4, userUpdateView.getId());

            try {
                connection.setAutoCommit(false); //autoCommit false
                try (PreparedStatement ps = connection.prepareStatement(checkIfExists, Statement.RETURN_GENERATED_KEYS)) {
                    ps.setLong(1, userUpdateView.getId());
                    ps.execute();
                } catch (SQLException e) {
                    throw new DataAccessException("This guest for edit do not exists.");
                }

                int affectedRows = preparedStatement.executeUpdate();

                if (affectedRows == 0) {
                    throw new DataAccessException("Creating guest failed, no rows affected.");
                }
                connection.commit();    //Commit transaction
            } catch (SQLException e) {
                connection.rollback();  //rollback transaction
            } finally {
               connection.setAutoCommit(true); //autoCommit true
            }
        } catch (SQLException e) {
            throw new DataAccessException("Creating person failed operation on the database failed.");
        }
    }

    public List<UserReadView> findUserReadView(String name, String surname) {
        String insertGuestSQL = "SELECT guest_id, name, surname, date_of_birth" +
                " FROM booking.guests g" +
                " WHERE g.name = ? and g.surname = ?";
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(insertGuestSQL, Statement.RETURN_GENERATED_KEYS)){
             preparedStatement.setString(1, name);
             preparedStatement.setString(2, surname);

            ResultSet resultSet = preparedStatement.executeQuery();
                List<UserReadView> userReadView = new ArrayList<>();

                while (resultSet.next()) {
                    userReadView.add(mapToUserReadView(resultSet));
                }
                return userReadView;
            } catch (SQLException e) {
            throw new DataAccessException("User read view could not be loaded.", e);
        }
    }

    private UserReadView mapToUserReadView(ResultSet rs) throws SQLException {
        UserReadView userReadView = new UserReadView();
        userReadView.setId(rs.getLong("guest_id"));
        userReadView.setName(rs.getString("name"));
        userReadView.setSurname(rs.getString("surname"));
        userReadView.setDateOfBirth(rs.getString("date_of_birth"));

        return userReadView;
    }

   /* public PersonDetailView findPersonDetailedView(Long personId) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT id_person, email, given_name, family_name, nickname, city, house_number, street" +
                             " FROM bds.person p" +
                             " LEFT JOIN bds.address a ON p.id_address = a.id_address" +
                             " WHERE p.id_person = ?")
        ) {
            preparedStatement.setLong(1, personId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToPersonDetailView(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find person by ID with addresses failed.", e);
        }
        return null;
    }

    private UserReadView mapToPersonDetailView(ResultSet rs) throws SQLException {
        UserReadView userReadView = new UserReadView();
        userReadView.setId(rs.getLong("id_person"));
        userReadView.setEmail(rs.getString("email"));
        userReadView.setGivenName(rs.getString("given_name"));
        userReadView.setFamilyName(rs.getString("family_name"));
        userReadView.setNickname(rs.getString("nickname"));
        userReadView.setCity(rs.getString("city"));
        userReadView.sethouseNumber(rs.getString("house_number"));
        userReadView.setStreet(rs.getString("street"));
        return personDetailView;
    }*/

}
