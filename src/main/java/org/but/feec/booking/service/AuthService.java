package org.but.feec.booking.service;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.but.feec.booking.api.UserAuthView;
import org.but.feec.booking.data.UserRepository;
import org.but.feec.booking.exceptions.ResourceNotFoundException;

public class AuthService {
    private UserRepository userRepository;

    public AuthService(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    private UserAuthView findPersonByLogin(String login) {
        return userRepository.findPersonByLogin(login);
    }

    public boolean authenticate(String login, String password) {
        if (login == null || login.isEmpty() || password == null || password.isEmpty()) {
            return false;
        }

        UserAuthView userAuthView = findPersonByLogin(login);
        if (userAuthView == null) {
            throw new ResourceNotFoundException("Provided login is not found.");
        }

        BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), userAuthView.getPassword());
        System.out.println("verified: " + result.verified);
        return result.verified;
    }

   public static void main(String[] args) {
        String password = "heslo";
        String bcryptHashString = BCrypt.withDefaults().hashToString(12, password.toCharArray());
        System.out.println(bcryptHashString);
        BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), bcryptHashString.toCharArray());
        System.out.println(result);
        // UPDATE .. persons WHERE SET
    }

}
