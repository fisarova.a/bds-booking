package org.but.feec.booking.service;

import org.but.feec.booking.api.UserCreateView;
import org.but.feec.booking.api.UserDeleteView;
import org.but.feec.booking.api.UserReadView;
import org.but.feec.booking.api.UserUpdateView;
import org.but.feec.booking.data.UserRepository;

import java.util.List;

public class UserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void createGuest(UserCreateView userCreateView) {
        userRepository.createGuest(userCreateView);
    }

    public void updateGuest(UserUpdateView userUpdateView) {
        userRepository.updateGuest(userUpdateView);
    }

    public void deleteGuest(UserDeleteView userDeleteView) {
        userRepository.deleteGuest(userDeleteView);
    }

    //public void findGuest(UserReadView userReadView){userRepository.findUserReadView(userReadView);}

   public List<UserReadView> findUserReadView(String name, String surname) {
        return userRepository.findUserReadView(name, surname);
    }

}
