package org.but.feec.booking;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * @author Anežka Fišarová
 */
public class App extends Application {

    private FXMLLoader loader;
    private VBox mainStage;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        try {
            loader = new FXMLLoader(getClass().getResource("Login.fxml"));
            mainStage = loader.load();

            primaryStage.setTitle("BDS JavaFX Booking");
            Scene scene = new Scene(mainStage);
            //setUserAgentStylesheet(STYLESHEET_MODENA);
            //String myStyle = getClass().getResource("css/myStyle.css").toExternalForm();
            //scene.getStylesheets().add(myStyle);

            primaryStage.setScene(scene);
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch (Exception ex) {
            //ExceptionHandler.handleException(ex);
        }
    }

}
