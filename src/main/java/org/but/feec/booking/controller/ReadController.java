package org.but.feec.booking.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.but.feec.booking.api.UserCreateView;
import org.but.feec.booking.api.UserReadView;
import org.but.feec.booking.data.UserRepository;
import org.but.feec.booking.service.UserService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Date;
import java.util.List;

public class ReadController {

    private static final Logger logger = LoggerFactory.getLogger(ReadController.class);

    @FXML
    public TextField nameTextField;
    @FXML
    public TextField surnameTextField;
    @FXML
    private TableView readTableView;
    @FXML
    private TableColumn<UserReadView, Long> guestsId;
    @FXML
    private TableColumn<UserReadView, String> guestsName;
    @FXML
    private TableColumn<UserReadView, String> guestsSurname;
    @FXML
    private TableColumn<UserReadView, String> guestsDateOfBirth;
    @FXML
    private Button findButton;

    private UserService userService;
    private UserRepository userRepository;
    private ValidationSupport validation;

    @FXML
    public void initialize() {
        userRepository = new UserRepository();
        userService = new UserService(userRepository);

        validation = new ValidationSupport();
        validation.registerValidator(nameTextField, Validator.createEmptyValidator("The name must not be empty."));
        validation.registerValidator(surnameTextField, Validator.createEmptyValidator("The surname must not be empty."));

        findButton.disableProperty().bind(validation.invalidProperty());



        guestsId.setCellValueFactory(new PropertyValueFactory<UserReadView, Long>("guest_id"));
        guestsName.setCellValueFactory(new PropertyValueFactory<UserReadView, String>("name"));
        guestsSurname.setCellValueFactory(new PropertyValueFactory<UserReadView, String>("surname"));
        guestsDateOfBirth.setCellValueFactory(new PropertyValueFactory<UserReadView, String>("date_of_birth"));

        //readTableView.getSortOrder().add(guestsId);

        logger.info("ReadController initialized");
    }

    @FXML
    void findActionHandler(ActionEvent event){
        String name = nameTextField.getText();
        String surname = surnameTextField.getText();
        ObservableList observableList = initializeUserData(name, surname);
        //readTableView.setItems(observableList);  //nefunguje TODO

            }

  private ObservableList<UserReadView> initializeUserData(String name, String surname) {
        List<UserReadView> guests = userService.findUserReadView(name, surname);
        return FXCollections.observableArrayList(guests);
    }
}
