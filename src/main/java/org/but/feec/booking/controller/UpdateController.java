package org.but.feec.booking.controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.util.Duration;
import org.but.feec.booking.api.UserUpdateView;
import org.but.feec.booking.data.UserRepository;
import org.but.feec.booking.service.UserService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Date;
import java.util.Optional;

public class UpdateController {

    private static final Logger logger = LoggerFactory.getLogger(UpdateController.class);

    @FXML
    public TextField idTextField;
    @FXML
    public TextField nameTextField;
    @FXML
    private TextField surnameTextField;
    @FXML
    private TextField dateOfBirthTextField;
    @FXML
    private Button updateButton;

    private UserService userService;
    private UserRepository userRepository;
    private ValidationSupport validation;

    @FXML
    public void initialize() {
        userRepository = new UserRepository();
        userService = new UserService(userRepository);

        validation = new ValidationSupport();
        validation.registerValidator(idTextField, Validator.createEmptyValidator("The id must not be empty."));
        validation.registerValidator(nameTextField, Validator.createEmptyValidator("The id must not be empty."));
        validation.registerValidator(surnameTextField, Validator.createEmptyValidator("The id must not be empty."));
        validation.registerValidator(dateOfBirthTextField, Validator.createEmptyValidator("The id must not be empty."));

        updateButton.disableProperty().bind(validation.invalidProperty());

        logger.info("UpdateController initialized");
    }

    @FXML
    public void updateActionHandler(ActionEvent event) {
        Long id = Long.valueOf(idTextField.getText());
        String name = nameTextField.getText();
        String surname = surnameTextField.getText();
        String dateOfBirth = dateOfBirthTextField.getText();

        UserUpdateView userUpdateView = new UserUpdateView();
        userUpdateView.setId(id);
        userUpdateView.setName(name);
        userUpdateView.setSurname(surname);
        userUpdateView.setDateOfBirth(Date.valueOf(dateOfBirth));

        userService.updateGuest(userUpdateView);

        userEditedConfirmationDialog();
    }
    private void userEditedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Guest Edited Confirmation");
        alert.setHeaderText("Your guest was successfully edited.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }

}
