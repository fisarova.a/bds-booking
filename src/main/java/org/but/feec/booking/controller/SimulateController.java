package org.but.feec.booking.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimulateController {

    private static final Logger logger = LoggerFactory.getLogger(SimulateController.class);

    @FXML
    public TextField sqlTextField;
    @FXML
    public TableView simulateTable;
    @FXML
    private Button simulateButton;



    @FXML
    void simulateActionHandler(ActionEvent event){
        handleSimulate();
    }

    private void handleSimulate() {
        System.out.println("hello");
    }
}
