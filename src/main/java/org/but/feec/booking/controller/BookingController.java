package org.but.feec.booking.controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Duration;
//import org.but.feec.javafx.api.PersonCreateView;
//import org.but.feec.javafx.data.PersonRepository;
//import org.but.feec.javafx.services.PersonService;
import org.but.feec.booking.App;
import org.but.feec.booking.data.UserRepository;
import org.but.feec.booking.service.UserService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Optional;

public class BookingController {

    private static final Logger logger = LoggerFactory.getLogger(BookingController.class);

    @FXML
    public Button createButton;
    @FXML
    private Button readButton;
    @FXML
    private Button updateButton;
    @FXML
    private Button deleteButton;
    @FXML
    private Button findAllButton;
    @FXML
    private Button simulateAttackButton;
    @FXML
    public Button filterButton;
    @FXML
    public TextField sqlTextField;
    @FXML
    public TableView sqlTable;

    private UserService userService;
    private UserRepository userRepository;
    private ValidationSupport validation;

    @FXML
    void handleCreateNewGuest(ActionEvent event) {
        handleCreate();
    }
    @FXML
    void handleReadBooking(ActionEvent event) {
        handleRead();
    }
    @FXML
    void handleUpdateGuest(ActionEvent event) {
        handleUpdate();
    }
    @FXML
    void handleDeleteGuest(ActionEvent event) {
        handleDelete();
    }
    @FXML
    void handleSimulateAttack(ActionEvent event) {
        handleSimulate();
    }
    @FXML
    void handleFilterBooking(ActionEvent event) {
        handleFilter();
    }

    private void fxmlLoader(String fxmlName){
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/" + fxmlName + ".fxml"));
            Scene scene = new Scene(fxmlLoader.load());
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Booking "+ fxmlName);
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    private void handleCreate(){
        fxmlLoader("Create");
    }
    private void handleRead(){
        fxmlLoader("Read");
    }
    private void handleUpdate(){
        fxmlLoader("Update");
    }
    private void handleDelete(){
        fxmlLoader("Delete");
    }
    private void handleSimulate(){
        fxmlLoader("SimulateAttack");
    }
    private void handleFilter(){

    }

}
