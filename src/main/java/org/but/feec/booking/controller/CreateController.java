package org.but.feec.booking.controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.util.Duration;
import org.but.feec.booking.api.UserCreateView;
import org.but.feec.booking.data.UserRepository;
import org.but.feec.booking.service.UserService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Date;
import java.util.Optional;


public class CreateController {

    private static final Logger logger = LoggerFactory.getLogger(CreateController.class);

    @FXML
    public TextField nameTextField;
    @FXML
    private TextField surnameTextField;
    @FXML
    private TextField dateOfBirthTextField;
    @FXML
    private Button createButton;

    private UserService userService;
    private UserRepository userRepository;
    private ValidationSupport validation;

    @FXML
    public void initialize() {
        userRepository = new UserRepository();
        userService = new UserService(userRepository);

        validation = new ValidationSupport();
        validation.registerValidator(nameTextField, Validator.createEmptyValidator("The name must not be empty."));
        validation.registerValidator(surnameTextField, Validator.createEmptyValidator("The surname must not be empty."));
        validation.registerValidator(dateOfBirthTextField, Validator.createEmptyValidator("The date od birth must not be empty."));

        createButton.disableProperty().bind(validation.invalidProperty());

        logger.info("CreateController initialized");
    }

    @FXML
    void createActionHandler(ActionEvent event){
        String name = nameTextField.getText();
        String surname = surnameTextField.getText();
        String dateOfBirth = dateOfBirthTextField.getText();

        UserCreateView userCreateView = new UserCreateView();
        userCreateView.setName(name);
        userCreateView.setSurname(surname);
        userCreateView.setDateOfBirth(Date.valueOf(dateOfBirth));

        userService.createGuest(userCreateView);
        userCreatedConfirmationDialog();
    }

    private void userCreatedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("User Created Confirmation");
        alert.setHeaderText("Your quest was successfully created.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }

}
