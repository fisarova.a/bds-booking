package org.but.feec.booking.controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.but.feec.booking.App;
import org.but.feec.booking.data.UserRepository;
import org.but.feec.booking.exceptions.DataAccessException;
import org.but.feec.booking.exceptions.ResourceNotFoundException;
import org.but.feec.booking.service.AuthService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Optional;

public class LoginController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @FXML
    public Label loginLabel;
    @FXML
    public Label passwordLabel;
    @FXML
    public Label logo;
    @FXML
    public VBox vbox;
    @FXML
    private Button signInButton;
    @FXML
    private TextField loginTextField;
    @FXML
    private PasswordField passwordTextField;

    private UserRepository userRepository;
    private AuthService authService;

    private ValidationSupport validation;

    public LoginController() {
    }

    @FXML
    private void initialize() {
        logger.info("initialize called");
        loginTextField.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                handleSignIn();
            }
        });
        passwordTextField.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                handleSignIn();
            }
        });
        initializeServices();
        initializeValidations();

        logger.info("LoginController initialized");
    }

    private void initializeValidations() {
        validation = new ValidationSupport();
        validation.registerValidator(loginTextField, Validator.createEmptyValidator("The login must not be empty."));
        validation.registerValidator(passwordTextField, Validator.createEmptyValidator("The password must not be empty."));
        signInButton.disableProperty().bind(validation.invalidProperty());
    }

    private void initializeServices() {
        userRepository = new UserRepository();
        authService = new AuthService(userRepository);
    }

    private void initializeLogos() {
        Image bedLogo = new Image(App.class.getResourceAsStream("logos/bedLogo-512px"));
        ImageView bedLogoImage = new ImageView(bedLogo);
        bedLogoImage.setFitHeight(85);
        bedLogoImage.setFitWidth(150);
        bedLogoImage.setPreserveRatio(true);
        logo.setGraphic(bedLogoImage);
    }
    public void signInActionHandler(ActionEvent actionEvent) {
        handleSignIn();
    }

    private void handleSignIn() {
        String login = loginTextField.getText();
        String password = passwordTextField.getText();

        System.out.println("Login " + login);
        System.out.println("Password " + password);

       try {
            boolean authenticated = authService.authenticate(login, password);
            if (authenticated) {
                System.out.println(".....................open........................");
                showBookingView();
            } else {
                showInvalidPaswordDialog();
            }
        } catch (ResourceNotFoundException | DataAccessException e) {
            showInvalidPaswordDialog();
        }

    }


    private void showBookingView() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("fxml/Booking.fxml"));
            Scene scene = new Scene(fxmlLoader.load());
            Stage stage = new Stage();
            stage.setResizable(false);
            stage.setTitle("Booking");
            stage.setScene(scene);

            Stage stageOld = (Stage) signInButton.getScene().getWindow();
            stageOld.close();
            authConfirmDialog();

            stage.show();
        } catch (IOException ex) {
            //ExceptionHandler.handleException(ex);
        }
    }

    private void showInvalidPaswordDialog() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Unauthenticated");
        alert.setHeaderText("The user is not authenticated");
        alert.setContentText("Please provide a valid login and password");//ww  w . j  a  va2s  .  co  m

        alert.showAndWait();
    }


    private void authConfirmDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Logging confirmation");
        alert.setHeaderText("You were successfully logged in.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();

        Optional<ButtonType> result = alert.showAndWait();

        if (result.get() == ButtonType.OK) {
            System.out.println("ok clicked");
        } else if (result.get() == ButtonType.CANCEL) {
            System.out.println("cancel clicked");
        }
    }

    public void handleOnEnterActionPassword(ActionEvent dragEvent) {
        handleSignIn();
    }

}

